<?php
  function safeFile($file) {  
    $lower = strtolower($file);
    $trim = rtrim($lower, '.');
    $regex = array('#[^A-Za-z0-9\.\_\- ]#');
    return trim(preg_replace($regex, '', $trim));       
  }

  $fname  = 'photos';
  $fname .= DIRECTORY_SEPARATOR;
  $fname .= safeFile($_POST['name']);
  $fname .= DIRECTORY_SEPARATOR;
  $fname .= safeFile(basename($_FILES['file']['name']));

  mkdir(dirname($fname), 0777, true);

  $uniq_fname = $fname;
  
  for($i=0;file_exists($uniq_fname);$i++){
    $uniq_fname  = pathinfo($fname, PATHINFO_DIRNAME);
    $uniq_fname .= DIRECTORY_SEPARATOR;
    $uniq_fname .= pathinfo($fname, PATHINFO_FILENAME);
    $uniq_fname .= '.' . $i . '.';
    $uniq_fname .= pathinfo($fname, PATHINFO_EXTENSION);
  }

  if (!move_uploaded_file($_FILES['file']['tmp_name'], $uniq_fname)) {
    header("Content-Type:text/plain");
    header("HTTP/1.1 500 Internal Server Error");
    echo "Failed to move uploaded file :(";
  }
?>
